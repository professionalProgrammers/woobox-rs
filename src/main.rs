fn main() {
    let client = woobox::Client::new();
    let mut poll_data = client.get_poll().unwrap();
    poll_data.password = Some("touchdown".to_string());
    dbg!(&poll_data);
    client.submit_vote(&poll_data);

    println!("Hello, world!");
}
