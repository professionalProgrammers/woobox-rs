use reqwest::multipart;
use select::document::Document;
use select::predicate::*;

pub type WooResult<T> = Result<T, WooError>;

#[derive(Debug)]
pub enum WooError {
    Network,
    InvalidBody,
}

pub struct Client {
    client: reqwest::Client,
}

impl Client {
    pub fn new() -> Self {
        Client {
            client: reqwest::Client::new(),
        }
    }

    pub fn get_poll(&self) -> WooResult<PollData> {
        let res = self
            .client
            .post("https://woobox.com/paav6j")
            .send()
            .map_err(|_| WooError::Network)?;

        let doc = Document::from_read(res).map_err(|_| WooError::InvalidBody)?;
        let mut script_data: String = doc
            .find(Name("script"))
            .filter_map(|e| e.find(Text).last())
            .filter_map(|e| e.as_text())
            .take(1)
            .collect();
        script_data += "'';var options_session_data = options.session_data; var options_facebook_appid = options.facebook_appid; var options_offer_url = options.offer_url;";

        let mut vm = rapidus::vm::vm::VM::new();
        let mut parser = rapidus::parser::Parser::new("main", &script_data);
        let node = parser.parse_all().unwrap();
        let func_info = vm.compile(&node, true).unwrap();
        vm.run_global(func_info).unwrap();

        let session: rapidus::vm::jsvalue::value::Value = vm
            .current_context
            .variable_environment
            .get_value("options_session_data")
            .unwrap_or(rapidus::vm::jsvalue::value::Value::undefined().into())
            .into();

        let id: rapidus::vm::jsvalue::value::Value = vm
            .current_context
            .variable_environment
            .get_value("options_facebook_appid")
            .unwrap_or(rapidus::vm::jsvalue::value::Value::undefined().into())
            .into();

        let url: rapidus::vm::jsvalue::value::Value = vm
            .current_context
            .variable_environment
            .get_value("options_offer_url")
            .unwrap_or(rapidus::vm::jsvalue::value::Value::undefined().into())
            .into();

        Ok(PollData {
            password: None,
            id: id.to_string(),
            session: session.to_string(),
            url: url.to_string(),
        })
    }

    pub fn submit_vote(&self, data: &PollData) -> WooResult<()> {
        let form = multipart::Form::new()
            .text("offer", "paav6j")
            .text("action", "submitattempts")
            .text("ci_session", data.session.to_string());

        let mut res = self
            .client
            .post("https://woobox.com/conversion")
            .multipart(form)
            .send()
            .map_err(|_| WooError::Network)?;
        dbg!(&res);
        let text = res.text().map_err(|_| WooError::Network)?;
        dbg!(text);

        let form = multipart::Form::new()
            .text("password", data.password.as_ref().unwrap().to_string())
            .text("appid", data.id.to_string())
            .text("actions", "generate")
            .text("submitted", "1")
            .text("ci_session", data.session.to_string())
            .text("vote", "1"); // Option #

        let mut res = self
            .client
            .post(&data.url)
            .multipart(form)
            .send()
            .map_err(|_| WooError::Network)?;

        dbg!(&res);
        let text = res.text().map_err(|_| WooError::Network)?;
        dbg!(text);
        Ok(())
    }
}

#[derive(Debug)]
pub struct PollData {
    pub password: Option<String>,
    id: String,
    session: String,
    pub url: String,
}
